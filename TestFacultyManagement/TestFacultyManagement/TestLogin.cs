﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using FacultyManagement.Controllers;
using FacultyManagement.Services;
using FacultyManagement.ViewModel;
using FacultyManagement.Repositories;
using System.Web.Security;

namespace UnitTestCRUD
{
    /// <summary>
    /// Summary description for TestLogin
    /// </summary>
    [TestClass]
    public class TestLogin : Controller
    {
        private LoginService serv;
        private HomeController ctrl;

        [TestInitialize]
        public void TestLoginSetUp()
        {
            FacultyManagement.App_Start.AutoMapperConfig.CreateMaps();
            this.serv = new LoginService(new AdminStaffRepository(), new TeacherRepository(), new CODRepository(), new StudentRepository());
            this.ctrl=new HomeController(serv);
        }

        [TestMethod]
        public void TestLogin1()
        {
            LoginViewModel loginViewModel = new LoginViewModel { Username = "amy", Password = "balabala", TypeOfUser = UserType.Student };
            bool torf = serv.ValidateUser(ref loginViewModel);
            Assert.IsTrue(torf, "Student not login correctly");
        }

        [TestMethod]
        public void TestRecoverPassword()
        {
            RecoverPasswordViewModel recoverPasswordViewModel = new RecoverPasswordViewModel { Username = "amy", Email = "yejin.ro@gmail.com", TypeOfUser = UserType.Student };
            string oldPassword = (serv.GetRepositoryUser(recoverPasswordViewModel.Username, recoverPasswordViewModel.TypeOfUser)).Password;
            this.ctrl.RecoverPassword(recoverPasswordViewModel);
            string newPassword = (serv.GetRepositoryUser(recoverPasswordViewModel.Username, recoverPasswordViewModel.TypeOfUser)).Password;
            Assert.IsTrue(oldPassword != newPassword, "Password not recover correctly");
        }

        [TestMethod]

        public void TsetChangePassword()
        {
            ChangePasswordViewModel passwordViewModel = new ChangePasswordViewModel { Username = "amy", ResetCode = "balabala", NewPassword = "balalala", TypeOfUser = UserType.Student };
            string oldPassword = (serv.GetRepositoryUser(passwordViewModel.Username, passwordViewModel.TypeOfUser)).Password;
            this.ctrl.ChangePassword(passwordViewModel);
            string newPassword = (serv.GetRepositoryUser(passwordViewModel.Username, passwordViewModel.TypeOfUser)).Password;
            Assert.IsTrue(oldPassword != newPassword, "Password not changed correctly");
        }
        
    }
}
