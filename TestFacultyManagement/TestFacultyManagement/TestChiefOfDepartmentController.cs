﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FacultyManagement.Controllers;
using FacultyManagement.ViewModel;
using FacultyManagement.Models.Faculty;
using FacultyManagement.Services;
using FacultyManagement.Repositories;

namespace UnitTestCRUD
{
    /// <summary>
    /// Summary description for TestChiefOfDepartmentController
    /// </summary>
    [TestClass]
    public class TestChiefOfDepartmentController : Controller
    {
        private ChiefOfDepartmentController ctrl;

        [TestInitialize]
        public void TestControllerSetUp()
        {
            FacultyManagement.App_Start.AutoMapperConfig.CreateMaps();
            this.ctrl = new ChiefOfDepartmentController();
        }

        [TestMethod]
        public void TestControllerCreat()
        {
            TeacherViewModel teacherViewModel = new TeacherViewModel { Name = "amy", Username = "amy", Password = "balabala", Department = new Department { Name = "mathematics" } };
            this.ctrl.Create(teacherViewModel);
            TeacherService serv = new TeacherService(new TeacherRepository());
            TeacherViewModel addedteacher = serv.GetAll().Find(std => std.Username.Equals(teacherViewModel.Username));
            Assert.IsTrue(addedteacher.Username.Equals(teacherViewModel.Username), "Teacher not created correctly");
        }

        [TestMethod]
        public void TestControllerEdit()
        {
            TeacherService serv = new TeacherService(new TeacherRepository());
            TeacherViewModel teacherViewModel = serv.FindById(10);
            teacherViewModel.Name = "jame";
            this.ctrl.Edit(teacherViewModel);
            TeacherViewModel editedteacher = serv.GetAll().Find(std => std.Username.Equals(teacherViewModel.Username));
            Assert.IsTrue(editedteacher.Username.Equals(teacherViewModel.Username), "Teacher not Edit correctly");
        }

        [TestMethod]
        public void TestControllerDelete()
        {
            this.ctrl.DeleteConfirmed(11);
            TeacherService serv = new TeacherService(new TeacherRepository());
            TeacherViewModel deletedteacher = serv.GetAll().Find(std => std.Id == 11);
            Assert.IsTrue((deletedteacher == null), "Teacher not deleted correctly");
        }
    }
}
