﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FacultyManagement.Controllers;
using FacultyManagement.ViewModel;
using FacultyManagement.Models.YearOfStudy;
using FacultyManagement.Services;
using FacultyManagement.Repositories;


namespace UnitTestCRUD
{
    [TestClass]
    public class TestAdministrativeStaffController : Controller
    {
        private AdministrativeStaffController ctrl;

        [TestInitialize]
        public void TestControllerSetUp()
        {
            FacultyManagement.App_Start.AutoMapperConfig.CreateMaps();
            this.ctrl = new AdministrativeStaffController();
        }

        [TestMethod]
        public void TestControllerCreat()
        {
            StudentViewModel StudentViewModel = new StudentViewModel { Name = "amy", Username = "amy", Password = "balabala", Group = new StudentsGroup { Name = "923" } };
            this.ctrl.Create(StudentViewModel);
            StudentService serv = new StudentService(new StudentRepository());
            StudentViewModel addedStudent = serv.GetAll().Find(std => std.Username.Equals(StudentViewModel.Username));
            Assert.IsTrue(addedStudent.Username.Equals(StudentViewModel.Username), "Student not created correctly");
        }

        [TestMethod]
        public void TestControllerEdit()
        {
            StudentService serv = new StudentService(new StudentRepository());
            StudentViewModel StudentViewModel = serv.FindById(1015);
            StudentViewModel.Name = "jame";
            this.ctrl.Edit(StudentViewModel);
            StudentViewModel editedStudent = serv.GetAll().Find(std => std.Username.Equals(StudentViewModel.Username));
            Assert.IsTrue(editedStudent.Username.Equals(StudentViewModel.Username), "Student not Edit correctly");
        }

        [TestMethod]
        public void TestControllerDelete()
        {
            this.ctrl.DeleteConfirmed(1018);
            StudentService serv = new StudentService(new StudentRepository());
            StudentViewModel deletedStudent = serv.GetAll().Find(std => std.Id == 1018);
            Assert.IsTrue((deletedStudent == null), "Student not deleted correctly");
            //Assert.IsNull(deletedStudent);
        }

    }
}
