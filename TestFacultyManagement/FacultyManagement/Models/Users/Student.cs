﻿using FacultyManagement.Models.YearOfStudy;

namespace FacultyManagement.Models.Users
{
	public class Student : IUser
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public string Username { get; set; }

		public string Password { get; set; }

		public virtual StudentsGroup Group { get; set; }
	}
}