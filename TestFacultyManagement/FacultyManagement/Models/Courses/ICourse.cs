﻿using System.ComponentModel.DataAnnotations;
using FacultyManagement.Models.YearOfStudy;

namespace FacultyManagement.Models.Courses
{
	public interface ICourse
	{
		[Key]
		int Id { get; set; }

		[Required]
		string Name { get; set; }

		[Required]
		int Credits { get; set; }

		Semester Semester { get; set; }
	}
}
