﻿using System.ComponentModel.DataAnnotations;

namespace FacultyManagement.ViewModel
{
	public class LoginViewModel
	{
		[Required(ErrorMessage = "Username is required!")]
		public string Username { get; set; }

		[Required(ErrorMessage = "Password is required!")]
		public string Password { get; set; }

		public UserType TypeOfUser { get; set; }
	}

	public enum UserType
	{
		Teacher,
		Student,
		ChiefOfDepartment,
		AdministrativeStaff
	};
}