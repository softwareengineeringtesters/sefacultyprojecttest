﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using FacultyManagement.Models;
using FacultyManagement.Models.Users;

namespace FacultyManagement.Repositories
{
	public class CODRepository : IUserRepository<ChiefOfDepartment>
	{
		public void Add(ChiefOfDepartment user)
		{
			using (var context = new FacultyManagementContext())
			{
				context.ChiefsOfDepartments.AddOrUpdate(user);
				context.SaveChanges();
			}
		}

		public void Delete(int id)
		{
			using (var context = new FacultyManagementContext())
			{
				var userToDelete = this.FindById(id);
				if (userToDelete == null)
				{
					throw new KeyNotFoundException("User not found!");
				}
				context.ChiefsOfDepartments.Remove(userToDelete);
				context.SaveChanges();
			}
		}

		public void Update(ChiefOfDepartment newUser)
		{
			using (var context = new FacultyManagementContext())
			{
				context.ChiefsOfDepartments.AddOrUpdate(newUser);
				context.SaveChanges();
			}
		}

		public ChiefOfDepartment FindById(int id)
		{
			using (var context = new FacultyManagementContext())
			{
				return context.ChiefsOfDepartments.Find(id);
			}
		}

		public List<ChiefOfDepartment> GetAll()
		{
			using (var context = new FacultyManagementContext())
			{
				return context.ChiefsOfDepartments.ToList();
			}
		}
	}
}