﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using FacultyManagement.Models;
using FacultyManagement.Models.Courses;

namespace FacultyManagement.Repositories
{
	public class FacultativeCoursesRepository : ICourseRepository<FacultativeCourse>
	{
		public void Add(FacultativeCourse course)
		{
			using (var context = new FacultyManagementContext())
			{
				context.FacultativeCourses.AddOrUpdate(course);
				context.SaveChanges();
			}
		}

		public void Delete(int id)
		{
			using (var context = new FacultyManagementContext())
			{
				var courseToDelete = this.FindById(id);
				if (courseToDelete == null)
				{
					throw new KeyNotFoundException("Facultative Course not found!");
				}
				context.FacultativeCourses.Remove(courseToDelete);
				context.SaveChanges();
			}
		}

		public void Update(FacultativeCourse newCourse)
		{
			using (var context = new FacultyManagementContext())
			{
				context.FacultativeCourses.AddOrUpdate(newCourse);
				context.SaveChanges();
			}
		}

		public FacultativeCourse FindById(int id)
		{
			using (var context = new FacultyManagementContext())
			{
				return context.FacultativeCourses.Find(id);
			}
		}

		public List<FacultativeCourse> GetAll()
		{
			using (var context = new FacultyManagementContext())
			{
				return context.FacultativeCourses.ToList();
			}
		}
	}
}