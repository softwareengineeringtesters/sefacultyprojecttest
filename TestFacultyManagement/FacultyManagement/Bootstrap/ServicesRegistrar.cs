﻿using Autofac;
using FacultyManagement.Services;

namespace FacultyManagement.Bootstrap
{
	public class ServicesRegistrar
	{
		public static void RegisterWith(ContainerBuilder builder)
		{
			builder.RegisterType<LoginService>().SingleInstance();
		}
	}
}