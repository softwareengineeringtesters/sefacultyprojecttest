namespace FacultyManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Foreign : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdminStaffs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Username = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Teachers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Username = c.String(),
                        Password = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        Department_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departments", t => t.Department_Id)
                .Index(t => t.Department_Id);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Faculty_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Teachers", t => t.Id)
                .ForeignKey("dbo.Faculties", t => t.Faculty_Id)
                .Index(t => t.Id)
                .Index(t => t.Faculty_Id);
            
            CreateTable(
                "dbo.Faculties",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Graduates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Department_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departments", t => t.Department_Id)
                .Index(t => t.Department_Id);
            
            CreateTable(
                "dbo.Specialisations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Graduate_Id = c.Int(),
                        Undergraduate_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Graduates", t => t.Graduate_Id)
                .ForeignKey("dbo.Undergraduates", t => t.Undergraduate_Id)
                .Index(t => t.Graduate_Id)
                .Index(t => t.Undergraduate_Id);
            
            CreateTable(
                "dbo.StudyLanguages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.YearOfStudies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Semesters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StudentsGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        YearOfStudy_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.YearOfStudies", t => t.YearOfStudy_Id)
                .Index(t => t.YearOfStudy_Id);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Username = c.String(),
                        Password = c.String(),
                        Group_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StudentsGroups", t => t.Group_Id)
                .Index(t => t.Group_Id);
            
            CreateTable(
                "dbo.Undergraduates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Department_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departments", t => t.Department_Id)
                .Index(t => t.Department_Id);
            
            CreateTable(
                "dbo.FacultativeCourses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Credits = c.Int(nullable: false),
                        Semester_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Semesters", t => t.Semester_Id)
                .Index(t => t.Semester_Id);
            
            CreateTable(
                "dbo.MandatoryCourses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Credits = c.Int(nullable: false),
                        Semester_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Semesters", t => t.Semester_Id)
                .Index(t => t.Semester_Id);
            
            CreateTable(
                "dbo.OptionalCourses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Credits = c.Int(nullable: false),
                        Semester_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Semesters", t => t.Semester_Id)
                .Index(t => t.Semester_Id);
            
            CreateTable(
                "dbo.StudyLanguageSpecialisations",
                c => new
                    {
                        StudyLanguage_Id = c.Int(nullable: false),
                        Specialisation_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.StudyLanguage_Id, t.Specialisation_Id })
                .ForeignKey("dbo.StudyLanguages", t => t.StudyLanguage_Id, cascadeDelete: true)
                .ForeignKey("dbo.Specialisations", t => t.Specialisation_Id, cascadeDelete: true)
                .Index(t => t.StudyLanguage_Id)
                .Index(t => t.Specialisation_Id);
            
            CreateTable(
                "dbo.SemesterYearOfStudies",
                c => new
                    {
                        Semester_Id = c.Int(nullable: false),
                        YearOfStudy_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Semester_Id, t.YearOfStudy_Id })
                .ForeignKey("dbo.Semesters", t => t.Semester_Id, cascadeDelete: true)
                .ForeignKey("dbo.YearOfStudies", t => t.YearOfStudy_Id, cascadeDelete: true)
                .Index(t => t.Semester_Id)
                .Index(t => t.YearOfStudy_Id);
            
            CreateTable(
                "dbo.YearOfStudyStudyLanguages",
                c => new
                    {
                        YearOfStudy_Id = c.Int(nullable: false),
                        StudyLanguage_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.YearOfStudy_Id, t.StudyLanguage_Id })
                .ForeignKey("dbo.YearOfStudies", t => t.YearOfStudy_Id, cascadeDelete: true)
                .ForeignKey("dbo.StudyLanguages", t => t.StudyLanguage_Id, cascadeDelete: true)
                .Index(t => t.YearOfStudy_Id)
                .Index(t => t.StudyLanguage_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OptionalCourses", "Semester_Id", "dbo.Semesters");
            DropForeignKey("dbo.MandatoryCourses", "Semester_Id", "dbo.Semesters");
            DropForeignKey("dbo.FacultativeCourses", "Semester_Id", "dbo.Semesters");
            DropForeignKey("dbo.Undergraduates", "Department_Id", "dbo.Departments");
            DropForeignKey("dbo.Specialisations", "Undergraduate_Id", "dbo.Undergraduates");
            DropForeignKey("dbo.Teachers", "Department_Id", "dbo.Departments");
            DropForeignKey("dbo.Graduates", "Department_Id", "dbo.Departments");
            DropForeignKey("dbo.Specialisations", "Graduate_Id", "dbo.Graduates");
            DropForeignKey("dbo.YearOfStudyStudyLanguages", "StudyLanguage_Id", "dbo.StudyLanguages");
            DropForeignKey("dbo.YearOfStudyStudyLanguages", "YearOfStudy_Id", "dbo.YearOfStudies");
            DropForeignKey("dbo.StudentsGroups", "YearOfStudy_Id", "dbo.YearOfStudies");
            DropForeignKey("dbo.Students", "Group_Id", "dbo.StudentsGroups");
            DropForeignKey("dbo.SemesterYearOfStudies", "YearOfStudy_Id", "dbo.YearOfStudies");
            DropForeignKey("dbo.SemesterYearOfStudies", "Semester_Id", "dbo.Semesters");
            DropForeignKey("dbo.StudyLanguageSpecialisations", "Specialisation_Id", "dbo.Specialisations");
            DropForeignKey("dbo.StudyLanguageSpecialisations", "StudyLanguage_Id", "dbo.StudyLanguages");
            DropForeignKey("dbo.Departments", "Faculty_Id", "dbo.Faculties");
            DropForeignKey("dbo.Departments", "Id", "dbo.Teachers");
            DropIndex("dbo.YearOfStudyStudyLanguages", new[] { "StudyLanguage_Id" });
            DropIndex("dbo.YearOfStudyStudyLanguages", new[] { "YearOfStudy_Id" });
            DropIndex("dbo.SemesterYearOfStudies", new[] { "YearOfStudy_Id" });
            DropIndex("dbo.SemesterYearOfStudies", new[] { "Semester_Id" });
            DropIndex("dbo.StudyLanguageSpecialisations", new[] { "Specialisation_Id" });
            DropIndex("dbo.StudyLanguageSpecialisations", new[] { "StudyLanguage_Id" });
            DropIndex("dbo.OptionalCourses", new[] { "Semester_Id" });
            DropIndex("dbo.MandatoryCourses", new[] { "Semester_Id" });
            DropIndex("dbo.FacultativeCourses", new[] { "Semester_Id" });
            DropIndex("dbo.Undergraduates", new[] { "Department_Id" });
            DropIndex("dbo.Students", new[] { "Group_Id" });
            DropIndex("dbo.StudentsGroups", new[] { "YearOfStudy_Id" });
            DropIndex("dbo.Specialisations", new[] { "Undergraduate_Id" });
            DropIndex("dbo.Specialisations", new[] { "Graduate_Id" });
            DropIndex("dbo.Graduates", new[] { "Department_Id" });
            DropIndex("dbo.Departments", new[] { "Faculty_Id" });
            DropIndex("dbo.Departments", new[] { "Id" });
            DropIndex("dbo.Teachers", new[] { "Department_Id" });
            DropTable("dbo.YearOfStudyStudyLanguages");
            DropTable("dbo.SemesterYearOfStudies");
            DropTable("dbo.StudyLanguageSpecialisations");
            DropTable("dbo.OptionalCourses");
            DropTable("dbo.MandatoryCourses");
            DropTable("dbo.FacultativeCourses");
            DropTable("dbo.Undergraduates");
            DropTable("dbo.Students");
            DropTable("dbo.StudentsGroups");
            DropTable("dbo.Semesters");
            DropTable("dbo.YearOfStudies");
            DropTable("dbo.StudyLanguages");
            DropTable("dbo.Specialisations");
            DropTable("dbo.Graduates");
            DropTable("dbo.Faculties");
            DropTable("dbo.Departments");
            DropTable("dbo.Teachers");
            DropTable("dbo.AdminStaffs");
        }
    }
}
