﻿using System.Web.Mvc;
using FacultyManagement.Repositories;
using FacultyManagement.Services;
using FacultyManagement.ViewModel;

namespace FacultyManagement.Controllers
{
	public class ChiefOfDepartmentController : Controller
	{
		private readonly TeacherService teacherService = new TeacherService(new TeacherRepository());

		// GET: ChiefOfDepartment
		public ActionResult Index()
		{
			return View(this.teacherService.GetAll());
		}

		// GET: ChiefOfDepartment/Details/5
		public ActionResult Details(int id)
		{
			var teacherViewModel = this.teacherService.FindById(id);
			if (teacherViewModel == null)
			{
				return HttpNotFound();
			}
			return View(teacherViewModel);
		}

		// GET: ChiefOfDepartment/Create
		public ActionResult Create()
		{
			return View();
		}

		// POST: ChiefOfDepartment/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "Id,Name,Username,Department")] TeacherViewModel teacherViewModel)
		{
			if (!ModelState.IsValid) return View(teacherViewModel);
			try
			{
				this.teacherService.Add(teacherViewModel);
			}
			catch (DepartmentNotFoundException exc)
			{
				ModelState.AddModelError("", exc.Message);
				return View(teacherViewModel);
			}

			return RedirectToAction("Index");
		}

		// GET: ChiefOfDepartment/Edit/5
		public ActionResult Edit(int id)
		{
			var teacherViewModel = this.teacherService.FindById(id);
			if (teacherViewModel == null)
			{
				return HttpNotFound();
			}
			return View(teacherViewModel);
		}

		// POST: ChiefOfDepartment/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "Id,Name,Username,Department")] TeacherViewModel teacherViewModel)
		{
			if (!ModelState.IsValid) return View(teacherViewModel);
			try
			{
				this.teacherService.UpdateUser(teacherViewModel);
			}
			catch (DepartmentNotFoundException exc)
			{
				ModelState.AddModelError("", exc.Message);
				return View(teacherViewModel);
			}

			return RedirectToAction("Index");
		}

		// GET: ChiefOfDepartment/Delete/5
		public ActionResult Delete(int id)
		{
			var teacherViewModel = this.teacherService.FindById(id);
			if (teacherViewModel == null)
			{
				return HttpNotFound();
			}
			return View(teacherViewModel);
		}

		// POST: ChiefOfDepartment/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			this.teacherService.DeleteUser(id);
			return RedirectToAction("Index");
		}
	}
}
