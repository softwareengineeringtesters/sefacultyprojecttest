﻿using System.Web.Mvc;
using FacultyManagement.Repositories;
using FacultyManagement.Services;
using FacultyManagement.ViewModel;

namespace FacultyManagement.Controllers
{
	public class AdministrativeStaffController : Controller
	{
		private readonly StudentService studentService = new StudentService(new StudentRepository());

		// GET: AdministrativeStaff
		public ActionResult Index()
		{
			return View(this.studentService.GetAll());
		}

		// GET: AdministrativeStaff/Details/5
		public ActionResult Details(int id)
		{
			var studentViewModel = this.studentService.FindById(id);
			if (studentViewModel == null)
			{
				return HttpNotFound();
			}
			return View(studentViewModel);
		}

		// GET: AdministrativeStaff/Create
		public ActionResult Create()
		{
			return View();
		}

		// POST: AdministrativeStaff/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "Id,Name,Username,Password,Group")] StudentViewModel studentViewModel)
		{
			if (!ModelState.IsValid) return View(studentViewModel);

			this.studentService.Add(studentViewModel);
			
			return RedirectToAction("Index");
		}

		// GET: AdministrativeStaff/Edit/5
		public ActionResult Edit(int id)
		{
			var studentViewModel = this.studentService.FindById(id);
			if (studentViewModel == null)
			{
				return HttpNotFound();
			}
			return View(studentViewModel);
		}

		// POST: AdministrativeStaff/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "Id,Name,Username,Group")] StudentViewModel studentViewModel)
		{
			if (!ModelState.IsValid) return View(studentViewModel);
			try
			{
				this.studentService.Update(studentViewModel);
			}
			catch (StudentGroupNotFoundException exc)
			{
				ModelState.AddModelError("groupNotFound", exc.Message);
				return View(studentViewModel);
			}
			return RedirectToAction("Index");
		}

		// GET: AdministrativeStaff/Delete/5
		public ActionResult Delete(int id)
		{
			var studentViewModel = this.studentService.FindById(id);
			if (studentViewModel == null)
			{
				return HttpNotFound();
			}
			return View(studentViewModel);
		}

		// POST: AdministrativeStaff/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			this.studentService.Delete(id);
			return RedirectToAction("Index");
		}
	}
}
