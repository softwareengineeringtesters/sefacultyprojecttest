﻿using System.Web.Mvc;
using System.Web.Security;
using FacultyManagement.Services;
using FacultyManagement.ViewModel;

namespace FacultyManagement.Controllers
{
	public class HomeController : Controller
	{
		private readonly LoginService loginService;

		public HomeController(LoginService loginService)
		{
			this.loginService = loginService;
		}

		[HttpGet]
		public ActionResult Index()
		{
			return RedirectToAction("Login");
		}

		[HttpGet]
		public ActionResult Login()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Login(LoginViewModel user)
		{
			if (!ModelState.IsValid) return View();
			try
			{
				if (!this.loginService.ValidateUser(ref user))
				{
					ModelState.AddModelError("", "Incorrect username or password!"); 
					return View();
				}

				FormsAuthentication.SetAuthCookie(user.Username, false);
				return RedirectToAction("Index", user.TypeOfUser.ToString());
			}
			catch (UserNotFoundException)
			{
				ModelState.AddModelError("", "Incorrect username or password!");
				return View();	
			}
		}

		[HttpGet]
		public ActionResult RecoverPassword()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult RecoverPassword(RecoverPasswordViewModel recoverPasswordViewModel)
		{
			if (!ModelState.IsValid)
			{
				return View("RecoverPassword", recoverPasswordViewModel);
			}

			try
			{
				this.loginService.RecoverPassword(recoverPasswordViewModel);
			}
			catch (UserNotFoundException exc)
			{
				ModelState.AddModelError("", exc.Message);
				return View("RecoverPassword", recoverPasswordViewModel);
			}

			return View("ChangePassword", new ChangePasswordViewModel{Username = recoverPasswordViewModel.Username, TypeOfUser = recoverPasswordViewModel.TypeOfUser});
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult ChangePassword(ChangePasswordViewModel passwordViewModel)
		{
			try
			{
				var user = this.loginService.GetRepositoryUser(passwordViewModel.Username, passwordViewModel.TypeOfUser);
				if (this.loginService.IsResetCodeValid(user, passwordViewModel.ResetCode))
				{
					loginService.ResetPassword(user, passwordViewModel.NewPassword);
				}
			}
			catch (UserNotFoundException exc)
			{
				ModelState.AddModelError("", exc.Message);
			}
			return RedirectToAction("Login");
		}
	}
}
